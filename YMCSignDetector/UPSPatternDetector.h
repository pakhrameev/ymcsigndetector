//
//  UPSPatternDetector.h
//  YMCSignDetector
//
//  Created by Pavel Akhrameev on 23.10.16.
//  Copyright © 2016 Movavi. All rights reserved.
//


#import <Foundation/Foundation.h>


@class UIImage;
@class MSERFeature;


@interface UPSPatternDetector : NSObject

@property (nonatomic, copy) NSString *title;

@property (nonatomic, assign) NSUInteger maxNumberOfHolesDiff;
@property (nonatomic, assign) double maxConvexHullAreaRateDiff;
@property (nonatomic, assign) double maxMinRectAreaRateDiff;
@property (nonatomic, assign) double maxSkeletLengthRateDiff;
@property (nonatomic, assign) double maxContourAreaRateDiff;

@property (nonatomic, assign, readonly) double minContourAreaRateToIgnore;
@property (nonatomic, assign, readonly) double maxContourAreaRateToIgnore;

@property (nonatomic, copy, readonly) NSArray <UIImage *> *images;
@property (nonatomic, copy, readonly) NSArray <UIImage *> *maskImages;

/*
 Stores feature from the biggest MSER in the images
 */
- (void) learnImagesByNames: (NSArray <NSString *> *) imageNames;

/*
 Sum of differences between logo feature and given feature
 */
- (double) distance: (MSERFeature *) feature;

/*
 Returns true if the given feature is similar to the one learned from the template
 */
- (BOOL) isDesiredSign: (MSERFeature *) feature;

@end
