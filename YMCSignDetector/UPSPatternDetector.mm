//
//  UPSPatternDetector.m
//  YMCSignDetector
//
//  Created by Pavel Akhrameev on 23.10.16.
//  Copyright © 2016 Movavi. All rights reserved.
//


#import "UPSPatternDetector.h"
#import <UIKit/UIKit.h>
#import "ImageUtils.h"
#import "MSERManager.h"
#import "GeometryUtil.h"
#import "MSERFeature.h"


static NSString *const kMaskNameSuffix = @"_mask";


@interface UPSPatternDetector ()

@property (nonatomic, copy) NSArray <UIImage *> *images;
@property (nonatomic, copy) NSArray <UIImage *> *maskImages;
@property (nonatomic, copy) NSArray <MSERFeature *> *signs;

@end


@implementation UPSPatternDetector

#pragma mark - Properties

@synthesize maxNumberOfHolesDiff      = m_maxNumberOfHolesDiff;
@synthesize maxConvexHullAreaRateDiff = m_maxConvexHullAreaRateDiff;
@synthesize maxMinRectAreaRateDiff    = m_maxMinRectAreaRateDiff;
@synthesize maxSkeletLengthRateDiff   = m_maxSkeletLengthRateDiff;
@synthesize maxContourAreaRateDiff    = m_maxContourAreaRateDiff;


#pragma mark - Inits

- (instancetype) init
{
  if (self = [super init])
  {
    m_maxNumberOfHolesDiff = 0;
    m_maxConvexHullAreaRateDiff = 0.05;
    m_maxMinRectAreaRateDiff    = 0.05;
    m_maxSkeletLengthRateDiff   = 0.02;
    m_maxContourAreaRateDiff    = 0.1;
  }
  return self;
}

- (double) maxContourAreaRateToIgnore
{
  __block double maxContourAreaRateToIgnore = 0.0;
  [self.signs enumerateObjectsUsingBlock: ^(MSERFeature * _Nonnull sign,
                                            NSUInteger idx,
                                            BOOL * _Nonnull stop)
  {
    double minimum = sign.contourAreaRate - self.maxContourAreaRateDiff;
    
    if (idx == 0)
    {
      maxContourAreaRateToIgnore = minimum;
    }
    else
    {
      maxContourAreaRateToIgnore = MIN(minimum, maxContourAreaRateToIgnore);
    }
  }];
  return maxContourAreaRateToIgnore;
}

- (double) minContourAreaRateToIgnore
{
  __block double minContourAreaRateToIgnore = 0.0;
  [self.signs enumerateObjectsUsingBlock: ^(MSERFeature * _Nonnull sign,
                                            NSUInteger idx,
                                            BOOL * _Nonnull stop)
   {
     double maximum = sign.contourAreaRate + self.maxContourAreaRateDiff;
     
     if (idx == 0)
     {
       minContourAreaRateToIgnore = maximum;
     }
     else
     {
       minContourAreaRateToIgnore = MAX(maximum, minContourAreaRateToIgnore);
     }
   }];
  return minContourAreaRateToIgnore;
}


#pragma mark - Public methods

- (void) learnImagesByNames: (NSArray <NSString *> *) imageNames
{
  NSMutableArray <MSERFeature *> *msigns = [NSMutableArray new];
  NSMutableArray <UIImage *> *mimages = [NSMutableArray new];
  NSMutableArray <UIImage *> *mimageMasks = [NSMutableArray new];
  for (NSString *imgName in imageNames)
  {
    UIImage *img = [UIImage imageNamed: imgName];
    NSString *imgMaskName = [imgName stringByAppendingString: kMaskNameSuffix];
    UIImage *imgMask = [UIImage imageNamed: imgMaskName];
    
    MSERFeature *feature = [self learn: img];
    if ((feature != nil) && (img != nil) && (imgMask != nil))
    {
      [msigns addObject: feature];
      [mimages addObject: img];
      [mimageMasks addObject: imgMask];
    }
  }
  if (msigns.count > 0)
  {
    self.signs = msigns;
    self.images = mimages;
    self.maskImages = mimageMasks;
  }
}

- (MSERFeature *) learn: (UIImage *) templateImage
{
  cv::Mat logo = [ImageUtils cvMatFromUIImage: templateImage];
  
  //get gray image
  cv::Mat gray;
  cvtColor(logo, gray, CV_BGRA2GRAY);
  
  //mser with maximum area is
  std::vector<cv::Point> maxMser = [ImageUtils maxMser: &gray];
  
  //get 4 vertices of the maxMSER minrect
  cv::RotatedRect rect = cv::minAreaRect(maxMser);
  cv::Point2f points[4];
  rect.points(points);
  
  //normalize image
  cv::Mat M = [GeometryUtil getPerspectiveMatrix: points toSize: rect.size];
  cv::Mat normalizedImage = [GeometryUtil normalizeImage: &gray withTranformationMatrix: &M withSize: rect.size.width];
  
  //get maxMser from normalized image
  std::vector<cv::Point> normalizedMser = [ImageUtils maxMser: &normalizedImage];
  
  //remember the template
  return [[MSERManager sharedInstance] extractFeature: &normalizedMser];
}

- (double) distance: (MSERFeature *) feature
{
  __block double minDistance = FLT_MAX;
  for (MSERFeature *sign in self.signs)
  {
    double distance = [sign distance: feature];
    if (distance < minDistance)
    {
      minDistance = distance;
    }
  }
  return  minDistance;
}

- (BOOL) isDesiredSign: (MSERFeature *) feature
{
  for (MSERFeature *sign in self.signs)
  {
    if ([self feature: feature isEqualToSign: sign])
    {
      return YES;
    }
  }
  return NO;
}

- (BOOL) feature: (MSERFeature *) feature
   isEqualToSign: (MSERFeature *) sign
{
  if (std::labs(sign.numberOfHoles - feature.numberOfHoles) > self.maxNumberOfHolesDiff)
  {
    return NO;
  }
  
  if (fabs(sign.convexHullAreaRate - feature.convexHullAreaRate) > self.maxConvexHullAreaRateDiff)
  {
    //NSLog(@"convexHullAreaRate \t\t\t\t %f", fabs(sign.convexHullAreaRate - feature.convexHullAreaRate));
    return NO;
  }
  if (fabs(sign.minRectAreaRate - feature.minRectAreaRate) > self.maxMinRectAreaRateDiff)
  {
    //NSLog(@"minRectAreaRate \t\t\t\t %f", fabs(sign.minRectAreaRate - feature.minRectAreaRate));
    return NO;
  }
  if (fabs(sign.skeletLengthRate - feature.skeletLengthRate) > self.maxSkeletLengthRateDiff)
  {
    //NSLog(@"skeletLengthRate \t\t\t\t %f", fabs(sign.skeletLengthRate - feature.skeletLengthRate));
    return NO;
  }
  if (fabs(sign.contourAreaRate - feature.contourAreaRate) > self.maxContourAreaRateDiff)
  {
    //NSLog(@"contourAreaRate \t\t\t\t %f", fabs(sign.contourAreaRate - feature.contourAreaRate));
    return NO;
  }
  
  /*NSLog(@"------------------------------------------");
  NSLog(@"%@", [sign description]);
  NSLog(@"%@", [feature description]);
  NSLog(@"%@", [sign toString]);
  NSLog(@"%f \t %f \t %f \t %f",
        fabs(sign.convexHullAreaRate - feature.convexHullAreaRate),
        fabs(sign.minRectAreaRate - feature.minRectAreaRate),
        fabs(sign.skeletLengthRate - feature.skeletLengthRate),
        fabs(sign.contourAreaRate - feature.contourAreaRate)
        );*/
  
  return YES;
}

@end
