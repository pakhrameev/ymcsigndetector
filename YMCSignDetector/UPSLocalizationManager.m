//
//  UPSLocalizationManager.m
//  YMCMotionCounter
//
//  Created by Pavel Akhrameev on 22.10.16.
//  Copyright © 2016 Movavi. All rights reserved.
//

#import "UPSLocalizationManager.h"


@implementation UPSLocalizationManager

- (NSString *) localizedStringForKey: (NSString *) key
{
  return NSLocalizedStringFromTable(key, @"Localizable", nil);
}

+ (instancetype) sharedInstance
{
  static UPSLocalizationManager *instance = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    
    instance = [[self alloc] init];
  });
  
  return instance;
}

@end
