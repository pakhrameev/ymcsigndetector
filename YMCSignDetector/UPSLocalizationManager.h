//
//  UPSLocalizationManager.h
//  YMCMotionCounter
//
//  Created by Pavel Akhrameev on 22.10.16.
//  Copyright © 2016 Movavi. All rights reserved.
//


#import <Foundation/Foundation.h>


#define UPSLocalizedString(key) [[UPSLocalizationManager sharedInstance] localizedStringForKey: key]


@interface UPSLocalizationManager : NSObject

- (NSString *) localizedStringForKey: (NSString *) key;

+ (instancetype) sharedInstance;

@end
