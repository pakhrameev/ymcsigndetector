//
//  UPSSignDetector.m
//  YMCSignDetector
//
//  Created by Pavel Akhrameev on 27.10.16.
//  Copyright © 2016 Movavi. All rights reserved.
//


#import "UPSSignDetector.h"

#import <UIKit/UIKit.h>
#import "MSERManager.h"
#import "MSERFeature.h"
#import "ImageUtils.h"
#import "GeometryUtil.h"
#import "UPSPatternDetector.h"
#import "UPSLocalizationManager.h"

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>

#if (DEBUG)
#import "FPS.h"
#endif


@interface UPSSignDetector ()

@property (nonatomic, strong) UPSPatternDetector *railwayDetector;
@property (nonatomic, strong) UPSPatternDetector *noEntryDetector;
@property (nonatomic, strong) UPSPatternDetector *closedDetector;
@property (nonatomic, strong) UPSPatternDetector *oneDirectionDetector;
@property (nonatomic, strong) UPSPatternDetector *pedestrianDetector;
@property (nonatomic, copy) NSArray <UPSPatternDetector *> *detectors;

@property (nonatomic, assign, readonly) double minContourAreaRateToIgnore;
@property (nonatomic, assign, readonly) double maxContourAreaRateToIgnore;

@end


@implementation UPSSignDetector

#pragma mark - Properties

- (double) maxContourAreaRateToIgnore
{
  __block double maxContourAreaRateToIgnore = 0.0;
  [self.detectors enumerateObjectsUsingBlock: ^(UPSPatternDetector * _Nonnull detector,
                                                NSUInteger idx,
                                                BOOL * _Nonnull stop)
   {
     double minimum = detector.maxContourAreaRateToIgnore;
     
     if (idx == 0)
     {
       maxContourAreaRateToIgnore = minimum;
     }
     else
     {
       maxContourAreaRateToIgnore = MIN(minimum, maxContourAreaRateToIgnore);
     }
   }];
  return maxContourAreaRateToIgnore;
}

- (double) minContourAreaRateToIgnore
{
  __block double minContourAreaRateToIgnore = 0.0;
  [self.detectors enumerateObjectsUsingBlock: ^(UPSPatternDetector * _Nonnull detector,
                                                NSUInteger idx,
                                                BOOL * _Nonnull stop)
   {
     double maximum = detector.minContourAreaRateToIgnore;
     
     if (idx == 0)
     {
       minContourAreaRateToIgnore = maximum;
     }
     else
     {
       minContourAreaRateToIgnore = MAX(maximum, minContourAreaRateToIgnore);
     }
   }];
  return minContourAreaRateToIgnore;
}


#pragma mark - Public methods

- (void) processUIImage: (UIImage *) uiimage
             completion: (UPSSignDetectorCompletion) completion
{
  NSParameterAssert(completion != nil);
  if (completion == nil)
  {
    return;
  }
  
  __block cv::Mat image = [ImageUtils cvMatFromUIImage: uiimage];
  
  __weak UPSSignDetector *weakSelf = self;
  
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0UL), ^{
    
    [weakSelf processImage: image];
    
    UIImage *img = [ImageUtils UIImageFromCVMat: image];
    dispatch_async(dispatch_get_main_queue(), ^{
      
      completion (img);
    });
  });
}

- (void) processImage: (cv::Mat &) image
{
  if (self.detectors.count == 0)
  {
    [self configureDetectors];
  }
  
  cv::Mat gray;
  cvtColor(image, gray, CV_BGRA2GRAY);
  
  UPSPatternDetector *bestTemplateDetector = nil;
  double bestResultDistance = 0.6;
  cv::Point bestMatchLoc;
  cv::Mat bestTemplate;
  
  if (!self.useMser)
  {
    for (UPSPatternDetector *detector in self.detectors)
    {
      //NSString *internalStatus = [NSString stringWithFormat: UPSLocalizedString(@"STATUS_SIGN_DETECTION_MASK"), detector.title];
      //[self updateStatus: internalStatus];
      
      cv::Point matchLoc;
      cv::Mat templateMat;
      double bestTemplatedDistance = [self bestDistanceForDetector: detector
                                                       inImagePart: gray
                                                          matchLoc: matchLoc
                                                   withTemplateMat: templateMat
                                                    externalStatus: /*internalStatus*/nil];
      if (bestTemplatedDistance > bestResultDistance)
      {
        bestResultDistance = bestTemplatedDistance;
        bestTemplate = templateMat;
        bestMatchLoc = cv::Point(matchLoc.x, matchLoc.y);
        bestTemplateDetector = detector;
      }
    }
    
    if (bestTemplateDetector != nil)
    {
      //NSLog(@"minDist: %f", bestPoint);
      //cv::Rect bound = cv::boundingRect(*bestMser);
      //cv::rectangle(image, bound, sCYAN, 3);
      //[ImageUtils drawMser: bestMser intoImage: &image withColor: sRED];
      
      NSLog(@"maxMeasure: %f", bestResultDistance);
      cv::rectangle(image, cv::Rect(bestMatchLoc.x, bestMatchLoc.y, bestTemplate.rows, bestTemplate.cols), sCYAN, 3);
      
      UIImage *img = [ImageUtils UIImageFromCVMat: bestTemplate];
      
      if (self.delegate != nil)
      {
        dispatch_async(dispatch_get_main_queue(), ^{
          [self.delegate signDetector: self
                        didDetectSign: bestTemplateDetector.title
                          withMeasure: bestResultDistance
                       detectionImage: img];
        });
      }
    }
    else
    {
      UIImage *img = [ImageUtils UIImageFromCVMat: image];
      NSLog(@"%@", img);
      
      if (self.delegate != nil)
      {
        dispatch_async(dispatch_get_main_queue(), ^{
          [self.delegate signDetector: self
                        didDetectSign: @""
                          withMeasure: bestResultDistance
                       detectionImage: nil];
        });
      }
    }
    return;
  }
  
  std::vector<std::vector<cv::Point>> msers;
  [[MSERManager sharedInstance] detectRegions: gray intoVector: msers];
  
  NSLog(@"MSERs found: %@", @(msers.size()));
  
  if (msers.size() == 0)
  {
    return;
  }
  
  UPSPatternDetector *bestMserDetector = nil;
  std::vector <cv::Point> *bestMser = nil;
  const double pointBarrier = -10.0;
  double bestPoint = -10.0;
  
  for (UPSPatternDetector *detector in self.detectors)
  {
    std::vector <cv::Rect> rectsToSearchIn;
    
    //NSString *status = [NSString stringWithFormat: UPSLocalizedString(@"STATUS_SIGN_DETECTION_MASK"), detector.title];
    //[self updateStatus: status];
    
    std::for_each(msers.begin(), msers.end(), [&] (std::vector<cv::Point> &mser)
                  {
                    MSERFeature *feature = [[MSERManager sharedInstance] extractFeature: &mser];
                    
                    if (feature != nil)
                    {
                      if ([detector isDesiredSign: feature])
                      {
                        double tmp = [detector distance: feature];
                        BOOL betterThanPreviousMser = (bestPoint > tmp);
                        if (betterThanPreviousMser)
                        {
                          bestPoint = tmp;
                          bestMser = &mser;
                          bestMserDetector = detector;
                        }
                        if (( pointBarrier > tmp ) || ( betterThanPreviousMser ))
                        {
                          NSLog(@"tick: %@", @(bestPoint));
                          
                          cv::Rect bound = cv::boundingRect(*bestMser);
                          const CGFloat expand = 2.0;
                          int originx = MAX(0, bound.x - bound.width  * expand);
                          int originy = MAX(0, bound.y - bound.height * expand);
                          int width   = MIN(gray.cols - originx, bound.width  * (1.0 + 2.0 * expand));
                          int height  = MIN(gray.rows - originy, bound.height * (1.0 + 2.0 * expand));
                          cv::Rect searchBound = cv::Rect(originx,originy, width, height);
                          
                          cv::rectangle(image, bound, sGREEN, 3);
                          //cv::rectangle(image, searchBound, sYELLOW, 3);
                          
                          rectsToSearchIn.push_back(searchBound);
                        }
                      }
                    }
                  });
    //status = [NSString stringWithFormat: UPSLocalizedString(@"STATUS_MSERS_COUNT_MASK"), detector.title, @(rectsToSearchIn.size())];
    //[self updateStatus: status];
    
    // link together intersected rects
    
    std::vector <cv::Rect> searchedRects;
    
    for (auto rect : rectsToSearchIn)
    {
      BOOL alreadySearched = NO;
      for (auto r : searchedRects)
      {
        cv::Rect intersection = rect & r;
        if (intersection == rect)
        {
          //rect in r
          NSLog(@"skip");
          alreadySearched = YES;
          break;
        }
      }
      if (alreadySearched)
      {
        continue;
      }
      
      //NSString *internalStatus = [NSString stringWithFormat: UPSLocalizedString(@"STATUS_TEMPLATE_MATCHING_MASK"), status, @(searchedRects.size())];
      //[self updateStatus: internalStatus];
      
      searchedRects.push_back(rect);
      
      cv::Mat grayPart = cv::Mat(gray, rect);
      
      cv::rectangle(image, rect, sYELLOW, 3);
      
      cv::Point matchLoc;
      cv::Mat templateMat;
      double bestTemplatedDistance = [self bestDistanceForDetector: detector
                                                       inImagePart: grayPart
                                                          matchLoc: matchLoc
                                                   withTemplateMat: templateMat
                                                    externalStatus: /*internalStatus*/nil];
      if (bestTemplatedDistance > bestResultDistance)
      {
        bestResultDistance = bestTemplatedDistance;
        bestTemplate = templateMat;
        bestMatchLoc = cv::Point(matchLoc.x + rect.x, matchLoc.y + rect.y);
        bestTemplateDetector = detector;
      }
      
      [ImageUtils drawMser: bestMser intoImage: &image withColor: sRED];
    }
  }
  
#if (DEBUG)
  [FPS draw: image];
#endif
  
  if (bestTemplateDetector != nil)
  {
    //NSLog(@"minDist: %f", bestPoint);
    //cv::Rect bound = cv::boundingRect(*bestMser);
    //cv::rectangle(image, bound, sCYAN, 3);
    //[ImageUtils drawMser: bestMser intoImage: &image withColor: sRED];
    
    NSLog(@"maxMeasure: %f", bestResultDistance);
    cv::rectangle(image, cv::Rect(bestMatchLoc.x, bestMatchLoc.y, bestTemplate.rows, bestTemplate.cols), sCYAN, 3);
    
    UIImage *img = [ImageUtils UIImageFromCVMat: bestTemplate];
    
    if (self.delegate != nil)
    {
      dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate signDetector: self
                    didDetectSign: bestTemplateDetector.title
                        withMeasure: bestResultDistance
                     detectionImage: img];
      });
    }
  }
  else
  {
    UIImage *img = [ImageUtils UIImageFromCVMat: image];
    NSLog(@"%@", img);
    
    if (self.delegate != nil)
    {
      dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate signDetector: self
                      didDetectSign: bestMserDetector.title
                        withMeasure: bestResultDistance
                     detectionImage: nil];
      });
    }
  }
}


#pragma mark - Internal methods

- (double) bestDistanceForDetector: (UPSPatternDetector *) detector
                       inImagePart: (cv::Mat &) grayImagePart
                          matchLoc: (cv::Point &) matchLoc
                   withTemplateMat: (cv::Mat &) templateMat
                    externalStatus: (NSString *) externalStatus
{
  __block cv::Point bestMatchLoc;
  __block cv::Mat bestTemplate;
  __block double bestResultDistance = 0.0;
  NSUInteger imagesCount = detector.images.count;
  [detector.images enumerateObjectsUsingBlock: ^(UIImage * _Nonnull img,
                                                 NSUInteger idx,
                                                 BOOL * _Nonnull stop)
   {
     UIImage *maskImage = detector.maskImages[idx];
     
     cv::Point matchLoc;
     cv::Mat matchTemplate;
     
     //NSString *status = [NSString stringWithFormat: UPSLocalizedString(@"STATUS_IMAGE_INDEX_MASK"), @(idx + 1), @(imagesCount)];
     //status = [externalStatus stringByAppendingString: status];
     
     double bestDistance = [self bestDistanceForImage: img
                                            maskImage: maskImage
                                              inImage: grayImagePart
                                 previousBestDistance: bestResultDistance
                                             matchLoc: matchLoc
                                      withTemplateMat: matchTemplate
                                       externalStatus: /*status*/nil];
     if (bestDistance > bestResultDistance)
     {
       bestMatchLoc = matchLoc;
       bestResultDistance = bestDistance;
       bestTemplate = matchTemplate;
     }
   }];
  if (bestResultDistance > 0.0)
  {
    matchLoc = bestMatchLoc;
    templateMat = bestTemplate;
  }
  
  return bestResultDistance;
}

- (double) bestDistanceForImage: (UIImage *) templateImage
                      maskImage: (UIImage *) templateMaskImage
                        inImage: (const cv::Mat &) grayImage
           previousBestDistance: (double) previousBestDistance
                       matchLoc: (cv::Point &) matchLoc
                withTemplateMat: (cv::Mat &) templateMat
                 externalStatus: (NSString *) externalStatus
{
  const double maxDistanceToReach = 0.95;
  double bestDistance = 0.0;
  
  cv::Mat tmpltMat = [ImageUtils cvMatFromUIImage: templateImage];
  cv::Mat tmplatMaskMat = [ImageUtils cvMatFromUIImage:  templateMaskImage];
  
  cv::Mat gtmpltMat;
  cvtColor(tmpltMat, gtmpltMat, CV_BGRA2GRAY);
  cv::Mat gtmplatMaskMat;
  cvtColor(tmplatMaskMat, gtmplatMaskMat, CV_BGRA2GRAY);
  
  const NSUInteger minStep = 10;
  const NSUInteger maxDownscaling = 2;
  const CGFloat maxStretching = 3.f;
  const int minSide = MIN(grayImage.rows, grayImage.cols);
  const int endSide = self.useMser ? MAX(tmpltMat.rows / maxDownscaling, minSide / 2.0) : tmpltMat.rows / maxDownscaling;
  
  for (int i = minSide; i >= endSide; i -= minStep)
  {
    NSLog(@"--------------------------%d", i);
    
    //NSString *widthStatus = [NSString stringWithFormat: UPSLocalizedString(@"STATUS_WIDTH_INDEX_MASK"), @(i), @(minSide)];
    //widthStatus = [externalStatus stringByAppendingString: widthStatus];
    
    if (self.useMser)
    {
      //[self updateStatus: widthStatus];
    }
    
    for (int j = minSide; j >= endSide; j -= minStep)
    {
      NSLog(@"template size: %d, %d", i, j);
      
      if ((i * 1.0 / j > maxStretching) ||
          (j * 1.0 / i > maxStretching))
      {
        NSLog(@"Too big stretching");
        continue;
      }
      
      /*if (!self.useMser)
      {
        NSString *statusHeight = [NSString stringWithFormat: UPSLocalizedString(@"STATUS_HEIGHT_INDEX_MASK"), @(j), @(minSide)];
        NSString *status = [widthStatus stringByAppendingString: statusHeight];
        
        [self updateStatus: status];
      }*/
      
      cv::Size size(i,j);             //the dst image size,e.g.100x100
      
      cv::Mat gresizedTmpltMat;       //destination for template
      resize(gtmpltMat, gresizedTmpltMat, size);
      cv::Mat gresizedTmplatMaskMat;  //destination for template's mask
      resize(gtmplatMaskMat, gresizedTmplatMaskMat, size);
      
      cv::Mat res(grayImage.rows - gresizedTmpltMat.rows + 1, grayImage.cols - gresizedTmpltMat.cols + 1, CV_32FC1);
      cv::matchTemplate(grayImage, gresizedTmpltMat, res, CV_TM_CCORR_NORMED, gresizedTmplatMaskMat );
      //cv::threshold(res, res, MAX(previousBestDistance, bestDistance), 1., CV_THRESH_TOZERO);
      
      double minVal, maxVal;
      cv::Point  minLoc, maxLoc;
      
      cv::minMaxLoc(res, &minVal, &maxVal, &minLoc, &maxLoc, cv::Mat());
      
      double distance = maxVal;
      if (distance > bestDistance)
      {
        matchLoc = maxLoc;
        bestDistance = distance;
        templateMat = gresizedTmpltMat;
        
        //UIImage *img = [ImageUtils UIImageFromCVMat: templateMat];
        
        if (bestDistance > maxDistanceToReach)
        {
          return bestDistance;
        }
      }
    }
  }
  return bestDistance;
}

- (void) configureDetectors
{
  NSArray <NSString *> *railwaySignNames = @[
                                             @"2_1__1",
                                             //@"2_1__2",
                                             //@"2_1__3",
                                             //@"2_triangle",
                                             ];
  UPSPatternDetector *detector = [[UPSPatternDetector alloc] init];
  [detector learnImagesByNames: railwaySignNames];
  detector.maxConvexHullAreaRateDiff = 0.7;
  detector.maxMinRectAreaRateDiff = 0.7;
  detector.maxSkeletLengthRateDiff = 0.1;
  detector.maxContourAreaRateDiff = 0.7;
  detector.title = UPSLocalizedString(@"SIGN_1_2_TITLE");
  self.railwayDetector = detector;
  
  NSArray <NSString *> *noEntrySignNames = @[
                                             @"3_1__1",
                                             //@"3_1__2",
                                             //@"3_1__3",
                                             @"3_1__4",
                                             ];
  
  detector = [[UPSPatternDetector alloc] init];
  [detector learnImagesByNames: noEntrySignNames];
  detector.maxConvexHullAreaRateDiff = 0.7;
  detector.maxMinRectAreaRateDiff = 0.7;
  detector.maxSkeletLengthRateDiff = 0.1;
  detector.maxContourAreaRateDiff = 0.7;
  detector.title = UPSLocalizedString(@"SIGN_3_1_TITLE");
  self.noEntryDetector = detector;
  
  
  NSArray <NSString *> *closedSignNames = @[
                                            @"3_2__1",
                                            @"3_2__2",
                                            @"3_2__3",
                                            @"3_2__4",
                                            ];
  
  detector = [[UPSPatternDetector alloc] init];
  [detector learnImagesByNames: closedSignNames];
  detector.maxConvexHullAreaRateDiff = 0.02;
  detector.maxMinRectAreaRateDiff = 0.02;
  detector.maxSkeletLengthRateDiff = 0.01;
  detector.maxContourAreaRateDiff = 0.02;
  detector.title = UPSLocalizedString(@"SIGN_3_2_TITLE");
  self.closedDetector = detector;
  
  NSArray <NSString *> *oneDirectionSignNames = @[
                                                  @"5_5__1",
                                                  //@"5_5__2",
                                                  //@"5_5__3",
                                                  //@"5_5__4",
                                                  //@"5_5__5",
                                                  //@"5_5__6",
                                                  ];
  
  detector = [[UPSPatternDetector alloc] init];
  [detector learnImagesByNames: oneDirectionSignNames];
  detector.maxConvexHullAreaRateDiff = 0.7;
  detector.maxMinRectAreaRateDiff = 0.7;
  detector.maxSkeletLengthRateDiff = 0.1;
  detector.maxContourAreaRateDiff = 0.7;
  detector.title = UPSLocalizedString(@"SIGN_5_5_TITLE");
  self.oneDirectionDetector = detector;
  
  NSArray <NSString *> *pedestrianSignNames = @[
                                                //@"5_19_1__1",
                                                @"5_19_1__2",
                                                //@"5_19_1__3",
                                                //@"5_19_1__4",
                                                //@"5_19_1__5",
                                                @"5_19_2__1",
                                                //@"5_19_2__2",
                                                //@"5_19_2__3",
                                                //@"5_19_2__4",
                                                ];
  
  detector = [[UPSPatternDetector alloc] init];
  [detector learnImagesByNames: pedestrianSignNames];
  detector.maxConvexHullAreaRateDiff = 0.7;
  detector.maxMinRectAreaRateDiff = 0.7;
  detector.maxSkeletLengthRateDiff = 0.1;
  detector.maxContourAreaRateDiff = 0.7;
  detector.title = UPSLocalizedString(@"SIGN_5_19_1or2_TITLE");
  self.pedestrianDetector = detector;
  
  self.detectors = @[
                     self.railwayDetector,
                     self.noEntryDetector,
                     self.closedDetector,
                     self.oneDirectionDetector,
                     self.pedestrianDetector,
                     ];
  
  [MSERManager sharedInstance].minContourAreaRateToIgnore = self.minContourAreaRateToIgnore;
  [MSERManager sharedInstance].maxContourAreaRateToIgnore = self.maxContourAreaRateToIgnore;
}

- (void) updateStatus: (NSString *) status
{
  return;
  
  if (self.delegate != nil)
  {
    dispatch_async(dispatch_get_main_queue(), ^{
      [self.delegate signDetector: self
                  didUpdateStatus: status];
    });
  }
}

@end
