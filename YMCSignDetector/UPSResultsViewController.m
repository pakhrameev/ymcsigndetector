//
//  UPSResultsViewController.m
//  YMCSignDetector
//
//  Created by Pavel Akhrameev on 24.10.16.
//  Copyright © 2016 Movavi. All rights reserved.
//


#import "UPSResultsViewController.h"
#import "GMImagePickerController.h"
@import AudioToolbox;
@import AVFoundation;
#import "UPSSignDetector.h"
#import "UPSLocalizationManager.h"


@interface UPSResultsViewController () < GMImagePickerControllerDelegate,
                                         AVAudioPlayerDelegate,
                                         UPSSignDetectorDelegate >

@property (nonatomic, strong) AVAudioPlayer *silenceBgPlayer;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) UPSSignDetector *signDetector;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

@end


@implementation UPSResultsViewController


#pragma mark - Properties

@synthesize image = m_image;


- (void) setImage:(UIImage *)image
{
  if (m_image != image)
  {
    m_image = image;
    
    __weak UPSResultsViewController *weakSelf = self;
    [self.signDetector processUIImage: image
                           completion: ^(UIImage *image)
    {
      weakSelf.imageView.image = image;
    }];
  }
}


#pragma mark - Overrides

- (void) viewDidLoad
{
  [super viewDidLoad];
  
  self.statusLabel.text = @"";
  
  self.signDetector = [[UPSSignDetector alloc] init];
  self.signDetector.useMser = YES;
  self.signDetector.delegate = self;
  
  NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource: @"silence" ofType: @"mp3"]];
  self.silenceBgPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
  [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
  [[AVAudioSession sharedInstance] setActive: YES error: nil];
  [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
  self.silenceBgPlayer.numberOfLoops = -1;
  [self.silenceBgPlayer play];
  
  [UIApplication sharedApplication].idleTimerDisabled = YES;
}

- (void) viewWillAppear: (BOOL) animated
{
  [super viewWillAppear: animated];
  
  if (self.image == nil)
  {
    GMImagePickerController *picker = [[GMImagePickerController alloc] init];
    picker.delegate = self;
    picker.mediaTypes = @[ @(PHAssetMediaTypeImage) ];
    picker.customSmartCollections = nil;
    picker.allowsMultipleSelection = NO;
    [self presentViewController: picker animated: YES completion: nil];
  }
}


#pragma mark - UPSSignDetectorDelegate

- (void) signDetector: (UPSSignDetector *) sender
      didUpdateStatus: (NSString *) status
{
  self.statusLabel.text = status;
}

- (void) signDetector: (UPSSignDetector *) sender
        didDetectSign: (NSString *) sign
          withMeasure: (double) measure
       detectionImage: (UIImage *) image
{
  NSString *prefix = ((measure > 0.8) && (image != nil)) ? UPSLocalizedString(@"FOUND_SIGN") : UPSLocalizedString(@"CLOSEST_FOUND_SIGN");
  
  NSString *status = [prefix stringByAppendingString: sign];
  
  NSString *suffix = [NSString stringWithFormat: UPSLocalizedString(@"STATUS_MEASURE_MASK"), @(measure * 100)];
  
  status = [status stringByAppendingString: suffix];
  
  self.statusLabel.text = status;
}


#pragma mark - GMImagePickerControllerDelegate

- (void) assetsPickerController: (GMImagePickerController *) picker
         didFinishPickingAssets: (NSArray <PHAsset *> *) assets
{
  NSMutableArray *mimages = [NSMutableArray new];
  
  PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
  options.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
  options.synchronous = YES;
  
  PHImageManager *imageManager = [[PHImageManager alloc] init];
  for (PHAsset *asset in assets)
  {
    [imageManager requestImageForAsset: asset
                            targetSize: CGSizeMake(asset.pixelWidth, asset.pixelHeight)
                           contentMode: PHImageContentModeDefault
                               options: options
                         resultHandler: ^(UIImage * _Nullable result,
                                          NSDictionary * _Nullable info)
    {
      if (result != nil)
      {
        [mimages addObject: result];
      }
    }];
  }
  if (mimages.count > 0)
  {
    self.image = mimages.firstObject;
  }
  
  [picker.presentingViewController dismissViewControllerAnimated: YES completion: nil];
}

- (void) assetsPickerControllerDidCancel: (GMImagePickerController *) picker
{
  [picker.presentingViewController dismissViewControllerAnimated: YES completion: nil];
  
  [self.navigationController popToRootViewControllerAnimated: YES];
}

@end
