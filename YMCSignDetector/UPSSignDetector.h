//
//  UPSSignDetector.h
//  YMCSignDetector
//
//  Created by Pavel Akhrameev on 27.10.16.
//  Copyright © 2016 Movavi. All rights reserved.
//


#import <Foundation/Foundation.h>


@class UIImage;

@protocol UPSSignDetectorDelegate;


typedef void (^UPSSignDetectorCompletion) (UIImage *);


@interface UPSSignDetector : NSObject

@property (nonatomic, weak) id <UPSSignDetectorDelegate> delegate;
@property (nonatomic, assign) BOOL useMser;

- (void) processUIImage: (UIImage *) image
             completion: (UPSSignDetectorCompletion) completion;

@end


@protocol UPSSignDetectorDelegate <NSObject>

- (void) signDetector: (UPSSignDetector *) sender
      didUpdateStatus: (NSString *) status;

- (void) signDetector: (UPSSignDetector *) sender
        didDetectSign: (NSString *) sign
          withMeasure: (double) measure
       detectionImage: (UIImage *) image;


@end
