//
//  UPSVideoCamera.m
//  YMCSignDetector
//
//  Created by Pavel Akhrameev on 23.10.16.
//  Copyright © 2016 Movavi. All rights reserved.
//


#import "UPSVideoCamera.h"


@interface UPSVideoCamera ()

@property (nonatomic, retain) CALayer *customPreviewLayer;

@end



@implementation UPSVideoCamera

@synthesize customPreviewLayer = _customPreviewLayer;

#pragma mark - Overrides

- (void) updateOrientation
{
  // nop
}

- (void) layoutPreviewLayer
{
  if (self.parentView != nil)
  {
    CALayer *layer = self.customPreviewLayer;
    CGRect bounds = self.customPreviewLayer.bounds;
    layer.position = CGPointMake(self.parentView.frame.size.width / 2.f, self.parentView.frame.size.height / 2.f);
    layer.bounds = bounds;
  }
}


@end
