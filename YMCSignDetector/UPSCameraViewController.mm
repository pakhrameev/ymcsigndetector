//
//  UPSCameraViewController.mm
//  YMCSignDetector
//
//  Created by Pavel Akhrameev on 23.10.16.
//  Copyright © 2016 Movavi. All rights reserved.
//

#import "UPSCameraViewController.h"
#import <AVFoundation/AVFoundation.h>

#import "UPSVideoCamera.h"
#import "UPSLocalizationManager.h"
#import "UPSSignDetector.h"
#import "ImageUtils.h"
#import "GeometryUtil.h"

#include "opencv2/opencv.hpp"
#import <opencv2/videoio/cap_ios.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>

#if (DEBUG)
#import "FPS.h"
#endif


static const NSUInteger kWidth = 480;
static const NSUInteger kHeight = 640;


@interface UPSCameraViewController () < CvVideoCameraDelegate >

@property (nonatomic, weak) IBOutlet UIButton *startPauseButton;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) UPSVideoCamera *camera;
@property (nonatomic, assign) BOOL started;
@property (nonatomic, strong) UPSSignDetector *signDetector;

@end


@implementation UPSCameraViewController

#pragma mark - Properties

@synthesize started = m_started;



#pragma mark - Overrides

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  self.signDetector = [UPSSignDetector new];
  
  // Camera
  self.camera = [[UPSVideoCamera alloc] initWithParentView: self.imageView];
  self.camera.defaultAVCaptureDevicePosition = AVCaptureDevicePositionBack;
  self.camera.defaultAVCaptureSessionPreset = AVCaptureSessionPreset640x480;
  self.camera.defaultAVCaptureVideoOrientation = AVCaptureVideoOrientationPortrait;
  self.camera.defaultFPS = 30;
  self.camera.grayscaleMode = NO;
  self.camera.delegate = self;
}

- (void) viewDidAppear: (BOOL) animated
{
  [super viewDidAppear: animated];
  
  self.navigationController.navigationBarHidden = YES;
  
#if (TARGET_OS_SIMULATOR)
  [self test];
  
  return;
#endif
  
  // http://stackoverflow.com/questions/20464631/detect-permission-of-camera-in-ios
  AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType: AVMediaTypeVideo];
  if (authStatus == AVAuthorizationStatusAuthorized)
  {
    // do your logic
    
    [self.camera start];
  }
  else if (authStatus == AVAuthorizationStatusDenied)
  {
    // denied
    
    [self test];
  }
  else if (authStatus == AVAuthorizationStatusRestricted)
  {
    // restricted, normally won't happen
    
    [self test];
  }
  else if (authStatus == AVAuthorizationStatusNotDetermined)
  {
    // not determined?!
    NSString *mediaType = AVMediaTypeVideo;
    
    [AVCaptureDevice requestAccessForMediaType: mediaType completionHandler: ^(BOOL granted)
    {
      dispatch_async(dispatch_get_main_queue(), ^{
        
        if (granted)
        {
          NSLog(@"Granted access to %@", mediaType);
          
          [self.camera start];
        }
        else
        {
          NSLog(@"Not granted access to %@", mediaType);
          
          [self test];
        }
      });
    }];
  }
  else
  {
    // impossible, unknown authorization status
    
    [self test];
  }
}

- (IBAction) startPauseButtonClick: (UIButton *) sender
{
  self.started = !self.started;
  
  [self updateButtonTitle];
}


#pragma mark - CvVideoCameraDelegate

- (void) processImage: (cv::Mat &) image
{
  if (!self.started)
  {
#if (DEBUG)
    [FPS draw: image];
#endif
    return;
  }
  
  UIImage *uiimage = [ImageUtils UIImageFromCVMat: image];
  
  __weak UPSCameraViewController *weakSelf = self;
  [self.signDetector processUIImage: uiimage
                         completion: ^(UIImage *img)
  {
    weakSelf.imageView.image = img;
  }];
}


#pragma mark - Internal methods

- (void) updateButtonTitle
{
  NSString *title = self.started ? UPSLocalizedString(@"STOP_BUTTON_TITLE") : UPSLocalizedString(@"DETECT_BUTTON_TITLE");
  [self.startPauseButton setTitle: title
                         forState:UIControlStateNormal];
}

- (void) test
{
  UIImage *testImage = [UIImage imageNamed: @"test_image"];
  
  NSAssert(testImage != nil, @"Test image should be nonnull!");
  if (testImage == nil)
  {
    return;
  }
  
  cv::Mat image = [ImageUtils cvMatFromUIImage: testImage];
  
  //get gray image
  cv::Mat gray;
  cvtColor(image, gray, CV_BGRA2GRAY);
  
  //mser with maximum area is
  std::vector<cv::Point> mser = [ImageUtils maxMser: &gray];
  
  //get 4 vertices of the maxMSER minrect
  cv::RotatedRect rect = cv::minAreaRect(mser);
  cv::Point2f points[4];
  rect.points(points);
  
  //normalize image
  cv::Mat m = [GeometryUtil getPerspectiveMatrix: points toSize: rect.size];
  cv::Mat normalizedImage = [GeometryUtil normalizeImage: &gray withTranformationMatrix: &m withSize: rect.size.width];
  
  //get maxMser from normalized image
  std::vector<cv::Point> normalizedMser = [ImageUtils maxMser: &normalizedImage];
  
  self.imageView.backgroundColor = [UIColor greenColor];
  self.imageView.contentMode = UIViewContentModeCenter;
  self.imageView.image = [ImageUtils UIImageFromCVMat: normalizedImage];
  
  self.started = YES;
  [self updateButtonTitle];
  
  [self processImage: image];
}

@end
