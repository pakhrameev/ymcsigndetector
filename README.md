# Yandex Mobile Contest #
Задание 3

Очень много дорожно-транспортных происшествий случается из-за того, что невнимательные водители не замечают дорожные знаки: едут куда не следует или, например, в положенном месте не пропускают пешеходов. Предлагаем вам поучаствовать в решении этой злободневной проблемы. Для этого необходимо разработать прототип приложения для мобильных устройств, распознающего некоторые важные знаки. От вас требуется реализовать как можно более точное распознавание знаков по изображению с выводом соответствующих названий.

Вот знаки, которые нужно различать (нумерация согласно ПДД):

* 1.2. «Железнодорожный переезд без шлагбаума»
* 3.1. «Въезд запрещён» (в народе «кирпич»)
* 3.2. «Движение запрещено»
* 5.5. «Дорога с односторонним движением»
* 5.19.1, 5.19.2 «Пешеходный переход»

Задача считается решённой, если приложение сможет распознать знаки на фотографиях, которые жюри поместит в галерею телефона.

-----

Работает так себе =)
MSER отрабатывает отлично, но плохо распознаёт размазанные знаки (может, стоило кластеризовать по цветам). Template matching расстроил ужасно: даже с маской (ради чего потребовался OpenCV 3) выдает малорелевантный результат. Но, welcome, можно посмотреть, иногда работает))

P.S. почему-то на моём iPhone 6 теперь сразу закрывается на OOM, но на iPhone 5c работает до результата.